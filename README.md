# Developer Guides
A repository for organizing style guides, templates, tools, and techniques for the UC San Diego Development and Web Services Team.

* [Protocol](protocol/README.md)
  * [Code Review](protocol/code_review.md)
  * [Git](protocol/git.md)
  * [New GitLab Project Setup](protocol/new_project.md)
* [Templates](templates/README.md)
  * [UC San Diego Copyright Disclosure Form](http://invent.ucsd.edu/invent/researchers/reporting-new-innovation/copyright-disclosure-form/)
  * [Copyright](templates/UC_Copyright_Notice.txt)

## Contributing to Developer Guides
Since this repository covers best practice recommendations for the entire team,
no MR will be merged until the entire team gives a :thumbsup:.

*Note: For all other project repositories, follow the standard workflow outlined
in [git](protocol/git.md) and [code review](protocol/code_review.md)*

## License
Distributed under [CC0 1.0 Universal](LICENSE)

## Credits
These guidelines derive from community standards whenever possible. The [Thoughtbot Guides](http://github.com/thoughtbot/guides) repository was a primary inspiration, as was the [Stanford Library DeveloperPlaybook](https://github.com/sul-dlss/DeveloperPlaybook)
