# Create New Project

A guide for creating a new GitLab project repository

## Project README.md
Each project should have a `README.md` file. See [Project README's][readme]

## Create GitLab Repository
1. Navigate to the [UCSD GitLab Development sub-project][gitlab-dev]
1. Authenticate with your UCSD Credentials using `Google`
1. Click `New Project`
1. Add `Project Name`, `Project Description`
1. Set the `Visibility Level` to `public` unless it has to be private
1. Do initialize with a `LICENSE` (`MIT`)
1. If you don't already have one, do initialize with a `LICENSE` (`MIT`)
1. Click `Create Project`

![New GitLab Project](./assets/new-gitlab-project.png)

Refresh the browser page to confirm the repository is populated on GitLab

## Configure Core Project Settings

### Get SSH key setup for Deployment (Optional)
> NOTE: Optional in some cases, such as deploying via Tomcat Manager over http(s)

In order to deploy applications via SSH, a keypair will need to be generated and
the private key added as a CI/CD environment variable. Work with Operations to
obtain these keys, generally one keypair per environment, and ensure the keys
are setup on the hosts that need to be deployed to.

> NOTE: we only are doing this because we're running all GitLab runners in our
local infrastructure, which we trust to deploy.

Go to Settings -> CI/CD:

![CI/CD Settings](./assets/ci-cd-settings.png)

1. Click `Expand` on the `Variables` section, and create a variables for each
   SSH private key you have (per environment) named, for example,
   `SSH_KEY_STAGING`, `SSH_KEY_PRODUCTION` with the value of the key pasted in.
1. Toggle the `Protected` radio button for each variable, ensuring this variable is only
   accessible in protected branches/tags/environments. Note: `trunk` should be
   setup as a [protected branch][gitlab-protected-branches].
1. Click `Save Variables`

### Test Coverage
1. Navigate to Settings -> CI/CD:
1. Scroll down to `Test coverage parsing`
1. Apply the regex that fits your test tooling (one of the examples listed
   should work for your language)
1. Click `Save Changes`
1. If you would like, add a coverage badge to the project `README`

![Test Coverage](./assets/coverage.png)

### Merge Requests
1. Navigate to Settings -> General
1. Toggle `Merge Requests`
1. Choose `Fast-forward merge`
1. Copy the contents of the [Gitlab Merge Request template][gitlab-mr-template]
   into the textarea titled `Default description template for merge requests`
   and adjust if needed.

### Merge Requests Approvals
1. Navigate to Settings -> General
1. Toggle `Merge request approvals`
1. Change `No. approvals required` from `0` to `1`

### CI/CD Runners
1. Navigate to Settings -> CI/CD
1. Toggle `Runners`
1. In the `Shared Runners` section, click `Disable shared Runners`
1. In the `Group Runners` section, enable the group shared Runner listed. This
   is our local set of GitLab Runners running in our own infrastructure.

## Integrations
### Slack (ChatOps, etc.)
1. Navigate to Settings -> Integrations
1. Scroll down and click `Slack Application`
1. Follow the steps to active the integration for your application
1. Now, if you have any `chatops` jobs setup in your GitLab CI file, you (or
   Operations) will be able to deploy in Slack:

Examples:
```
# Deploy the tag 1.5.0 to staging
/gitlab ucsdlibrary/apbatch run staging_deploy 1.5.0

# Deploy the trunk(default) branch to pontos
/gitlab ucsdlibrary/dmr run pontos_deploy
```

### Jira
1. Navigate to Settings -> Integrations
1. Scroll down and click `Jira`
1. Leave the selected checkboxes as-is
1. In `Web URL` enter: `https://ucsdlibrary.atlassian.net`
1. Leave `Jira API URL` blank
1. In `Username or Email` find the username in LastPass for `jira gmail`
1. In `Enter new pw or api token` find the api token in LastPass for `jira
   gmail`. This is the value to the right of `UCSDLibrarySAToken:` in the Notes
   field.
1. In `Transition ID(s)` enter: `31`. If this ends up not working, find an issue
   and grab the JIRA ID (example: `TDX-169`) and put it into the API URL as
   follows:
   `https://ucsdlibrary.atlassian.net/rest/api/2/issue/TDX-169/transitions`.
   Look for the `Done` entry, and find its `id` value.
1. Click `Test settings and save changes`

## Create .gitlab-ci.yml
Now we need to setup CI/CD for the project. There are a few scenarios outlined
below.

For new projects, you should create a Helm chart for the application unless
there is a good reason not to do so.

### Application Deployed via Helm
See the [Kubernetes and Helm][k8s-helm] file for more information about setting
up `helm` and `kubernetes` for your project.

### Ruby Application Deployed via Capistrano
1. Copy the [Ruby GitLab template][gitlab-ruby] into your project, renaming it
   to `.gitlab-ci.yml`
1. Update the template to match the needs of the project.
#### Capistrano Changes
There are a few other things you may need to change for migrating a project
using Capistrano.

- In `./config/deploy.rb` change `repo_url` to reference the new GitLab repo: `set :repo_url, 'https://gitlab.com/ucsdlibrary/development/dmr.git'`
- In each `./config/deploy/{environment}` file, you may have a block of code as
    below. This is no longer needed with the GitLab configuration, and may be
    removed. This is because the GitLab runner will have the key in its keychain
    already.

```
if ENV["CAP_SSHKEY_PONTOS"]
  puts "Using key: #{File.join(ENV["HOME"], ENV["CAP_SSHKEY_PONTOS"])}"
  set :ssh_options, { keys: File.join(ENV["HOME"], ENV["CAP_SSHKEY_PONTOS"]) }
end
```

### Java Application Deployed via Tomcat Manager
1. Copy the [Java Tomcat Manager GitLab template][gitlab-java-tomcat] into your project, renaming it
   to `.gitlab-ci.yml`
1. Update the template to match the needs of the project.
1. Add sensitive [environment variables][gitlab-env-vars] such as `TOMCAT_MANAGER_STAGING_PASSWORD` and others as needed to the GitLab project. You can follow the same steps as the SSH key setup, ensuring that the variable is `Protected` and, if possible, `Masked`. Non-sensitive environment variables can simply be added to the `.gitlab-ci.yml` file directly, or using an `.env` file.

### Java Application Deployed via SSH
> If this project **CAN** use Tomcat Manager, it would be preferrable to do that, as you can avoid direct SSH commands

1. Copy the [Java Tomcat Manager GitLab template][gitlab-java-tomcat] into your project, renaming it
   to `.gitlab-ci.yml`
1. Update the template to use direct SSH commands, mirroring the existing
   Jenkins set of commands. This would also include post-deploy commands such as
   restarting tomcat.

## Create RenovateBot Dependency Configuration
1. Speak with a GitLab admin (Dev Manager, Ops Manager, TDX Program Director)
   and ask them to go to [RenovateBot UI][renovate-ui] and add your project.
1. When you see the `Configure Renovate` MR show up in your repository, replace
   the contents of the `renovate.json` file with:

```
{
  "extends": [ "gitlab>ucsdlibrary/development/developer-guides" ]
}
```

This will use the `renovate.json` template defined in this repository. If you
need to make additional changes for your project, you can do so after extending
the base template as shown above. See the [Renovate Docs][renovate-docs] for
more information.

[gitlab-dev]:https://gitlab.com/ucsdlibrary/development
[gitlab-env-vars]:https://docs.gitlab.com/ee/ci/variables/
[gitlab-java-tomcat]:./templates/.gitlab-ci-java-tomcat.yml
[gitlab-mr-template]:./templates/gitlab_merge_requests.md
[gitlab-protected-branches]:https://gitlab.com/help/user/project/protected_branches.md
[gitlab-ruby]:./templates/.gitlab-ci-ruby.yml
[k8s-helm]:./protocol/kubernetes_helm.md
[renovate-docs]:https://docs.renovatebot.com/
[renovate-ui]:https://renovatebot.com/dashboard
