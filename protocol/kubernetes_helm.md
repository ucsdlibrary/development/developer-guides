# Kubernetes and Helm

A short introduction for setting up a project using `Kubernetes`(`k8s`) and the `Helm`
package manager.

First, it will be most efficient if you can get local installations of `kubectl`
and `helm`.

Currently you'll want:

| Tool | Version | Notes |
| ---- | ------- | ----- |
| `helm` | 3+ | Helm 2 is now legacy, ensure you start a new helm chart using v3 |
| `kubectl` | 1.16+ | There is a bit more flexibility in versions here. But, latest is best generally. |

## Dockerfile

The first step to getting a project ready to build and deploy to `Kubernetes` is
to create a [`Dockerfile`][dockerfile] for your project.

If this is a Ruby project, you may want to take a look at the [High
Five `Dockerfile`][highfive-dockerfile] or one of the Project Surfliner
`Dockerfile`s.

If this is a Java project, perhaps take a look at the [damsrepo
`Dockerfile`][damsrepo-dockerfile] and the [Chronopolis Dashboard
`Dockerfile`][chron-dashboard-dockerfile] for some initial inspiration.

## CI/CD
Once you have a working `Dockerfile`, you may want to setup a `build` job in
your `gitlab-ci.yml` file to start integrating changes you make to the
repository into the built container images.

High Five and Surfliner are good repositories to look at for inspiration in this
setup. You want to ensure you're leveraging pushing the built images to the
GitLab Image Registry, so that you can later use these images in `test` and
`deploy` jobs, as well as the Helm chart itself.

### Use gitlab-ci-tools templates
Unless you have a specific reason not to do so, you should use the templates
from our [gitlab-ci-tools][gitlab-ci-tools] repo in your `gitlab-ci.yml`.
Including these templates will give you `build` and `deploy` jobs that should
require minimal, to no, customization on your part. You can introduce them as
needed, so you can add the `deploy` template when you're ready.

Here is an example of including the `base` and `build` templates:

```
include:
  - project: 'ucsdlibrary/development/gitlab-ci-tools'
    ref: trunk
    file: 'gitlab-ci-base.yml'
  - project: 'ucsdlibrary/development/gitlab-ci-tools'
    ref: trunk
    file: 'gitlab-ci-kaniko-build.yml'
  - project: 'ucsdlibrary/development/gitlab-ci-tools'
    ref: trunk
```

### Common CI/CD Environment Variables
| Name                | Description                                                                                                                            |
| ------------------- | -------------------------------------------------------------------------------------------------------------------------------------- |
| `KUBE_ROLE_CONFIG`  | A `base64` encoded version of your role account's `KUBECONFIG` file. Use in CI/CD image to grant `helm` and `kubectl` access to deploy |
| `HELM_RELEASE_NAME` | This may simply be your application name, or you may choose to dynamically add the `sha` hash or merge id to separately identify it    |
| `KUBE_NAMESPACE`    | The k8s `namespace` that `helm` will deploy your application to                                                                        |
| `TLS_CRT`           | The TLS cert part of the keypair to setup `https`/`ssl` termination for your application                                               |
| `TLS_KEY`           | The TLS private key part of the keypair to setup `https`/`ssl` termination for your application                                        |

## Helm
Now you can start working towards a `Helm` chart for the project.
This is as simple as navigating into your project directory and running:

`helm create <chart-name>`

For example: `helm create dmr`

*Note*: The GitLab `deploy` template requires your Helm chart directory to be
named `chart`. So, after running the `helm create` command, please rename the
directory. So, from our `dmr` example above, we would:

```
mv dmr chart
```

We will not cover the entirety of `Helm` documentation here, as it is very well
documented on the [Helm website][helm-documentation].

Again, look to existing Helm charts for reference such as the Surfliner Projects
and High Five.

## Rancher
While you can install a local `kubernetes` cluster on your machine using tools
such as `minikube` and `k3s`, the simplest choice is probably to get a Rancher
`Project` and `Namespace`(s) setup that you can deploy to directly. This will
allow you to deploy your `Helm` chart directly onto the Rancher cluster, just as
you will ultimately do when setting up CI/CD for the application.

To ensure you have access, try logging on to the [UCSD Rancher][ucsd-rancher]
instance with your AD credentials. If you cannot log in, ask IT Operations for
assistance.

### Role Account
When you eventually deploy your application to Rancher/k8s using Helm, you will
need an account that has permissions to deploy to the specific k8s NameSpace
setup for the application. To facilitate this granular access control, ask
Operations to create a role account for the application.

Operations will place these credentials in LastPass, so that you can access them
there.

Operations should also create a Rancher Project for your application, and add
the role account as a member of that Project.

### Role Account KubeConfig
Now that you have a role account setup, log into Rancher with those credentials.
Ensure you select `Use a Local User`, so that your login screen looks like the
following:

![Rancher Local User Login](../assets/rancher-local-user.png)

Navigate to the `libcluster` [Dashboard][rancher-dashboard] and click the
`Kubeconfig File` button in the upper right of the screen.

![Rancher kubeconfig](../assets/rancher-kubeconfig.png)

Copy/Download the contents of this file onto your machine. Recommended location
is `$HOME/.kube/configs`.

Example: `$HOME/.kube/configs/rancher-dmr`

With that file in place, you can now tell `helm` and `kubectl` you want to use
that `kubeconfig` file by setting the `KUBECONFIG` environment variable.

Example: `export KUBECONFIG=~/.kube/configs/rancher-dmr`

This is particularly handy, because once you have a few projects with role
accounts, switching between them is as simple as exporting a different
`KUBECONFIG` environment variable value.

### Project Namespaces
While logged in as the role account, ensure you have namespaces setup that you
will need for your project.

Navigate to your Project, and then click on the `Namespaces` tab. From here you
might populate namespaces such as (using DMR as the example project still):

- `dmr-production`
- `dmr-staging`
- `dmr-review`

You are not limited to these namespaces, but this would cover the common CI/CD
environments you would deploy to.

## Helm Chart Development

The following are a few development tips the team has learned and compiled here.
If you have something else you're doing that you think others would benefit
from, add it here with an MR!

### Linting
It's very common when getting start with `Helm` charts, and `Kubernetes` `yaml`
files in general, to have simple mistakes with indentation, value assignment,
value scoping, etc.

One easy sanity check is to use the `helm lint` command. If there is anything
wrong that `Helm` can easily detect, it will tell you quickly.

### Debugging / Dry Runs
In a larger chart with subchart dependencies, such as `email`, `postgresql`,
`redis`, `solr`, etc.. there are going to be a number of variables being set.

If, for example, a hostname variable in your application chart for the
`postgresql` database doesn't line up to the actual `postgresql` database
hostname, you will get errors connecting to the database when the chart is
deployed.

To save time, it is often worth using the `helm --dry-run --debug ...` options
when testing out your chart before trying out a deployment, or as part of
tracking down errors. This will print out all the information that `helm` has
after processing all the templates and values provided. It does everything but
the actual deployment.

### Deploying without default values
Every `Helm` chart has a `values.yaml` file, which defines default values for
the chart. For an actual release, you will need to override some of those
default values, and often add additional values beyond that. There are two
options for this.

#### Using --set
`Helm` provides options for passing in values to override `values.yaml` via the
`--set` option. There is also `--set-string`. This is what is used in GitLab
CI/CD jobs. You can always copy the `deploy` invocation of `helm upgrade
--install` and update your values locally. This results in a very long command,
but will mirror your deploy job exactly.

Example from High Five's `deploy()` function:

```
    helm upgrade --install \
        --wait \
        --set image.repository="$CI_REGISTRY_IMAGE" \
        --set image.tag="$DEPLOY_TAG" \
        --set image.host="$APP_HOST" \
        --set email.sender="$EMAIL_SENDER" \
        --set email.bcc="$EMAIL_BCC" \
        --set ingress.enabled="true" \
        --set ingress.hosts[0].host="$APP_HOST" \
        --set ingress.hosts[0].paths[0]="/" \
        --set auth.method="$AUTH_METHOD" \
        --set auth.google_client_id="$GOOGLEAUTH_ID" \
        --set auth.google_client_secret="$GOOGLEAUTH_SECRET" \
        --set ldap.username="$LDAP_USER" \
        --set ldap.password="$LDAP_PASS" \
        ${first_release+--set-string load_ldap_employees="yes"} \
        --set global.postgresql.postgresqlUsername="$PG_USER" \
        --set global.postgresql.postgresqlPassword="$PG_PASS" \
        --set global.postgresql.postgresqlDatabase="$PG_DATABASE" \
        --set slack_webhook_url="$SLACK_WEBHOOK_URL" \
        ${TLS_CRT+--set ingress.tls[0].hosts[0]="$APP_HOST"} \
        ${TLS_CRT+--set ingress.tls[0].secretName="highfive-tls"} \
        ${TLS_CRT+--set tls.crt="$TLS_CRT"} \
        ${TLS_CRT+--set tls.key="$TLS_KEY"} \
        ${first_release+--set postgresql.initdbUser="$PG_USER"} \
        ${first_release+--set postgresql.initdbPassword="$PG_PASS"} \
        ${first_release+--values=hifive/postgres-load.yaml} \
        --namespace="$KUBE_NAMESPACE" \
        "$HELM_RELEASE_NAME" \
        hifive/
```

#### Use multiple values files
`Helm` also provides a `--values`(`-f`) option for passing in additional files
that contain values you want to override `values.yaml` defaults. You can pass
multiple files.

For example, given an additional file called `local-values.yaml`, you might run
the following:

```
helm upgrade --install --namespace=dmr-review --values=tmp/local-values.yaml review-test dmr/
```

This results in a much smaller `helm` command to manage. And allows you edit the
overridden values in a `yaml` file.

There is a downside to this, however. This file/use case often requires
sensitive values such as passwords. You should take care to ensure that you DO
NOT commit this file to the git repository accidentally. Perhaps put the file in
a directory that is `.gitignore`d such as `<project-root>/tmp`, or something.
Note that is the pattern used in the example above.

[chron-dashboard-dockerfile]:https://gitlab.com/chronopolis/chron-dashboard/-/blob/trunk/Dockerfile
[damsrepo-dockerfile]:https://gitlab.com/ucsdlibrary/development/damsrepo/-/blob/trunk/Dockerfile
[dockerfile]:https://docs.docker.com/engine/reference/builder/
[gitlab-ci-tools]:https://gitlab.com/ucsdlibrary/development/gitlab-ci-tools
[helm-documentation]:https://helm.sh/docs/
[highfive-dockerfile]:https://gitlab.com/ucsdlibrary/development/hifive/-/blob/trunk/Dockerfile
[rancher-dashboard]:https://lib-rancher-lb.ucsd.edu/c/c-28www/monitoring
[ucsd-rancher]:https://lib-rancher-lb.ucsd.edu/
